const { app, BrowserWindow } = require('electron');

/**
 * @type {BrowserWindow}
 */
let window;

app.on('ready', createWindow)

function createWindow() {
    window = new BrowserWindow({
        width: 1000,
        height: 600
    })
    window.webContents.openDevTools()
}

app.on('window-all-closed', () => {
    app.quit()
})
